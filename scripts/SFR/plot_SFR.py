def plot_SFR(UserFolder, UserLabels, folder_output):
    '''
    Plot star formation rate densities from A-SLOTH with observations
  
    Parameters
    ----------
        UserFolder: List[str]
            List of folder names to be plotted

        UserLabels: List[str]
            List of labels. If none, UserFolders are used as labels

        folder_output: str
            Folder in which plots will be saved

  
    Returns
    -------
        SFR_integrated: float
            in Msun/Mpc**3
    '''

    import os.path
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    from asloth_PlotConf import SetMyLayout
    from SFR.SFR_integral import SFR_integral

    # overwrite size for 2-panel figure
    fig = SetMyLayout(col=1, ratio=1.5)

    xmin = 0
    xmax = 32
    ymin = 1e-6
    ymax = 1
    unit = "$\mathrm{M}_\odot\,\mathrm{yr}^{-1}\,\mathrm{Mpc}^{-3}$"

# PopIII
    plt.subplot(2, 1, 1)
    plt.ylabel(unit)
    plt.yscale("log")
    plt.ylim((ymin, ymax))
    plt.xlim((xmin, xmax))
    plt.annotate("PopIII SFRD", (0.7*xmax, ymax*0.1))
    plt.tick_params(
      axis='x',           # changes apply to the x-axis
      labelbottom=False)  # labels along the bottom edge are off

    for folder, label in zip(UserFolder, UserLabels):
        SFR_file = folder+"/z_cSFR.dat"
        if os.path.isfile(SFR_file):
            print("The file "+SFR_file+" exists.")
        else:
            print("The file "+SFR_file+" does not exist.")
            return -99.

        data = np.genfromtxt(SFR_file, skip_header=2)

        # A-SLOTH
        z = data[:,0]
        cSFRII = data[:,3]  # PopII cosmic star formation rate density (Msun /yr /cMpc^3)
        cSFRIII = data[:,2]  # PopIII cosmic star formation rate density (Msun /yr /cMpc^3)

        if(len(label) < 3):  # otherwise too crowded
            plt.plot(z, cSFRII+cSFRIII, linewidth=1, linestyle='-.', color='grey', label=label+", total")
        else:
            plt.plot(z, cSFRII+cSFRIII, linewidth=1, linestyle='-.', color='grey')
        plt.plot(z, cSFRIII, label=label+", PopIII", linewidth=3)

        print("Cosmic PopIII stellar density from A-SLOTH:")
        SFR_integrated = SFR_integral(z, cSFRIII)
        print(SFR_integrated,"Msun/Mpc^3")
        if(False):#option to print additional vlaues from literature
            print("Cosmic PopIII stellar densities from literature:")
            print("S18:    289406 Msun/Mpc^3")
            print("J18:    217031 Msun/Mpc^3")
            print("dS14:   130310 Msun/Mpc^3")
            print("J13_III: 49613 Msun/Mpc^3")
            print("V20III:  38262 Msun/Mpc^3")
            print("T09III:   1913 Msun/Mpc^3")
            print("M18III:   1796 Msun/Mpc^3")


    # Pop II
    data_B15 = np.genfromtxt('Data/SFR/Behroozi15.dat')
    data_B19 = np.genfromtxt('Data/SFR/Behroozi19.dat')
    data_M14 = np.genfromtxt('Data/SFR/Madau14.dat')
    data_S18II = np.genfromtxt('Data/SFR/Sarmento19_PopII.dat') #https://ui.adsabs.harvard.edu/abs/2019ApJ...871..206S/abstract
    data_J13_II = np.genfromtxt('Data/SFR/Johnson13_PopII.dat') #https://arxiv.org/abs/1206.5824
    data_F16 = np.genfromtxt('Data/SFR/Finkelstein16.dat') #https://arxiv.org/abs/1511.05558
    data_T09II = np.genfromtxt('Data/SFR/Trenti09_PopII.dat') #https://arxiv.org/abs/0901.0711
    data_Xu16 = np.genfromtxt('Data/SFR/Xu16_tot.dat') #https://arxiv.org/abs/1604.07842 (Renaissance)
    data_M18II = np.genfromtxt('Data/SFR/Mebane18_PopII.dat') #https://arxiv.org/abs/1710.02528
    data_V20II = np.genfromtxt('Data/SFR/Visbal20_PopII.dat') #https://arxiv.org/abs/2001.11118

    # PopIII
    data_S18III = np.genfromtxt('Data/SFR/Sarmento19_PopIII.dat') #https://ui.adsabs.harvard.edu/abs/2019ApJ...871..206S/abstract
#    print("S18:",SFR_integral(data_S18III[:,0],data_S18III[:,1]),"Msun/Mpc^3")
    data_J13_III = np.genfromtxt('Data/SFR/Johnson13_PopIII.dat')#https://arxiv.org/abs/1206.5824
#    print("J13_III:",SFR_integral(data_J13_III[:,0],data_J13_III[:,1]),"Msun/Mpc^3")
    data_J18 = np.genfromtxt('Data/SFR/Jaacks18.dat')#https://arxiv.org/abs/1705.08059
#    print("J18:",SFR_integral(data_J18[:,0],data_J18[:,1]),"Msun/Mpc^3")
    data_dS14 = np.genfromtxt('Data/SFR/deSouza14.dat') #https://arxiv.org/abs/1401.2995 (average between min and max)
#    print("dS14:",SFR_integral(data_dS14[:,0],data_dS14[:,1]),"Msun/Mpc^3")
    data_T09III = np.genfromtxt('Data/SFR/Trenti09_PopIII.dat') #https://arxiv.org/abs/0901.0711
#    print("T09III:",SFR_integral(data_T09III[:,0],data_T09III[:,1]),"Msun/Mpc^3")
    data_M18III = np.genfromtxt('Data/SFR/Mebane18_PopIII.dat') #https://arxiv.org/abs/1710.02528
#    print("M18III:",SFR_integral(data_M18III[:,0],data_M18III[:,1]),"Msun/Mpc^3")
    data_V20III = np.genfromtxt('Data/SFR/Visbal20_PopIII.dat')#https://arxiv.org/abs/2001.11118
#    print("V20III:",SFR_integral(data_V20III[:,0],data_V20III[:,1]),"Msun/Mpc^3")


### POP III ###
#    plt.plot(z,cSFRII+cSFRIII,linewidth=1,linestyle=':',color='grey',label="A-SLOTH, total")
#    plt.plot(z,cSFRIII,label="A-SLOTH, PopIII",linewidth=3)
    plt.plot(data_S18III[:,0], data_S18III[:,1], label="Sarmento+19", linestyle='dashed')
    plt.plot(data_J13_III[:,0], data_J13_III[:,1], label="Johnson+13 (FiBY)", linestyle='dashed')
    plt.plot(data_J18[:,0], data_J18[:,1], label="Jaacks+18", linestyle='dashed')
    plt.plot(data_dS14[:,0], data_dS14[:,1], label="deSouza+14", linestyle='dashed')
    plt.plot(data_T09III[:,0], data_T09III[:,1], label="Trenti+09", linestyle='dashed')
    plt.plot(data_M18III[:,0], data_M18III[:,1], label="Mebane+18", linestyle='dashed')
    plt.plot(data_V20III[:,0], data_V20III[:,1], label="Visbal+20", linestyle='dashed')

    if(len(UserLabels)<4):
        plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.05), ncol=2, fancybox=True)


    ### POP II ###
    plt.subplot(2, 1, 2)
    plt.xlabel('redshift')
    plt.ylabel(unit)
    plt.yscale("log")
    plt.ylim((ymin, ymax))
    plt.xlim((xmin, xmax))
    plt.annotate("PopII SFRD", (0.7*xmax, ymax*0.1))


    for folder, label in zip(UserFolder, UserLabels):
        SFR_file = folder+"/z_cSFR.dat"
        if not os.path.isfile(SFR_file):
            print("The file "+SFR_file+" does not exist.")
            return -99.

        data = np.genfromtxt(SFR_file, skip_header=2)

        # A-SLOTH
        z = data[:,0]
        cSFRII = data[:,3] #  PopII cosmic star formation rate density (Msun /yr /cMpc^3)
        cSFRIII = data[:,2] #  PopIII cosmic star formation rate density (Msun /yr /cMpc^3)


        if(len(label) < 3): #  otherwise too crowded
            plt.plot(z, cSFRII+cSFRIII, linewidth=1, linestyle='-.', color='grey', label=label+", total")
        else:
            plt.plot(z, cSFRII+cSFRIII, linewidth=1, linestyle='-.', color='grey')
        plt.plot(z, cSFRII, label=label+", PopII", linewidth=3)

    plt.plot(data_S18II[:,0], data_S18II[:,1], label="Sarmento+19", linestyle='dashed')
    plt.plot(data_J13_II[:,0], data_J13_II[:,1], label="Johnson+13 (FiBY)", linestyle='dashed')
    plt.plot(data_T09II[:,0], data_T09II[:,1], label="Trenti+09", linestyle='dashed')
    plt.plot(data_Xu16[:,0], data_Xu16[:,1], label="Xu+16 (Ren. normal)", linestyle='dashed')
    plt.plot(data_M18II[:,0], data_M18II[:,1], label="Mebane+18", linestyle='dashed')
    plt.plot(data_V20II[:,0], data_V20II[:,1], label="Visbal+20", linestyle='dashed')


    plt.plot(data_F16[:,0], data_F16[:,1], label="Finkelstein16", linestyle=':')
    plt.plot(data_B15[:,0], data_B15[:,1], label="Behroozi+15", linestyle=':')
    plt.plot(data_B19[:,0], data_B19[:,1], label="Behroozi+19", linestyle=':')
    plt.plot(data_M14[:,0], data_M14[:,1], label="Madau&Dickinson14", linestyle=':')
    if(len(UserLabels)<4):
        plt.legend(loc='upper right', bbox_to_anchor=(1.0, -0.33), ncol=2, fancybox=True)

    plt.tight_layout()
    plt.savefig(folder_output+"z_SFRD.pdf", bbox_inches="tight")
    plt.clf()

    return SFR_integrated
