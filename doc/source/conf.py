# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
from glob import glob
import os
import sys
#add a-sloth main directory
sys.path.insert(0, os.path.abspath('../../.'))

#add directories with python scripts
sys.path.insert(0, os.path.abspath('../../scripts'))
sys.path.insert(0, os.path.abspath('../../scripts/tau'))
sys.path.insert(0, os.path.abspath('../../scripts/wrapper'))


# -- Project information -----------------------------------------------------

project = 'ASLOTH'
copyright = '2022, Mattis Magg, Tilman Hartwig, Li-Hsin Chen, Yuta Tarumi'
author = 'Mattis Magg, Tilman Hartwig, Li-Hsin Chen, Yuta Tarumi'

# The full version, including alpha/beta/rc tags
release = '1.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ["myst_parser",  'sphinx.ext.autodoc', 'sphinx.ext.coverage', 'sphinx.ext.napoleon', "sphinxfortran.fortran_domain", "fortran_autodoc"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

fortran_src_1 = [x[0]+"/" for x in os.walk("../../src")]
fortran_src = [os.path.join(*(x.split("/")[1:])) for x in fortran_src_1]
fortran_src = fortran_src + [os.path.join(*(x.split("/")[:])) for x in fortran_src_1]
fortran_src = fortran_src + ["../reindex/"]
print(fortran_src)
fortran_ext = ["F90", "f90"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
