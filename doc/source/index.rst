.. ASLOTH documentation master file, created by
   sphinx-quickstart on Fri Sep 24 13:46:07 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ASLOTH's documentation!
==================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

.. toctree::
   getting_started.md
   How-Tos/index.rst
   references.md
   code_doc/index.rst
   usage_policy.md
   help.md

