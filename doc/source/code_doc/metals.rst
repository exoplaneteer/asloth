
Metal modules
===============
Modules and functions for the handling of metals

.. toctree::
   :maxdepth: 1
   :caption: Contents:


Metals
------
.. f:automodule:: metals

Read Yields
-------------
.. f:automodule:: read_yields

Metal functions
---------------
.. f:automodule:: metal_functions


IGM Metallicity routines
------------------------
.. f:automodule:: igm_z

Metal Mix dZ
------------
.. f:automodule:: metalmix_dz

Metallicity distribution function
---------------------------------
.. f:automodule:: trace_mdf


