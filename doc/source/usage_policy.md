# Usage Policy

You can use A-SLOTH freely for scientific research and other applications in accordance with the license. If you publish results for which you have used A-SLOTH, you must cite the following two papers:
- Hartwig et al. 2022, submitted to ApJ
- Magg et al. 2022, submitted to JOSS

You do not have to invite the A-SLOTH developers as co-authors on your publications.
