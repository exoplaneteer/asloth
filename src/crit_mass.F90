#include "asloth.h"
module crit_masses
  use Numerical_Parameters
  use cosmic_time
  implicit none
  real, dimension(nlev_max), protected  :: mcrit
  real, dimension(nlev_max), protected :: Atomic_Cooling_Mass
contains

  subroutine set_mcrit()
      integer :: i
      do i=1, nlev
        mcrit(i) = get_crit_mass(zlev(i))
        Atomic_Cooling_Mass(i) = get_atomic_cooling_mass(zlev(i))
      end do
  end subroutine set_mcrit

  real function get_crit_mass(z)
      ! Determines critical mass for star formation at current redshift
      real, intent(in) :: z ! redshift
      real :: M_atomic_cooling
      if(lw_mode .eq. 5 .or. lw_mode .eq. 6) then
        get_crit_mass = get_Mcrit_Schauer(z)
      else
        get_crit_mass = get_Mcrit_Stacy(z)
      endif
      M_atomic_cooling = get_atomic_cooling_mass(z)
      get_crit_mass = MIN(get_crit_mass,M_atomic_cooling)
      return
  end function get_crit_mass


  real function get_Mcrit_Stacy(z)
      ! Modified to also check Jeans Mass calculated with modified speed of sound
      ! See Stacy et al. 2011
      real, intent(in) :: z ! redshift
      real v_stream_current, c_s_2, c_mod_2, crit_mass_stacy, T_b

      !for supersonic streaming baryon motion:
      v_stream_current = VBC*6.0e5/201.0*(1+z) !current streaming velocity in cm/s
      ! (VBC is relative to 1 sigma .i.e. 6 km/s @ z=200)
      T_b = 0.017*(1+z)*(1+z) ! Baryonic Temperature: Schneider ch. 10.3 pg 530
      !speed of sound squared
      c_s_2 = 5./3.*k_Boltzmann_erg*T_b/(MEAN_MOL*M_Atomic_g)
      !modified speend of sound squared:
      c_mod_2 = v_stream_current**2+c_s_2
      ! modified Jeans mass
      crit_mass_stacy = PI**(2.5)/6.0*(RHO_m_cgs*(1+z)**3)**(-0.5)/MSUN_cgs*(c_mod_2/G_cgs)**(1.5)

      ! Selecting maximum of cooling mass and baryon streaming Jeans mass:
      get_Mcrit_Stacy = max(get_M_vir(z, T_CRIT),crit_mass_stacy)
      return
  end function get_Mcrit_Stacy


  real function get_Mcrit_Schauer(z)
      !based on Schauer+21, MNRAS, Volume 507, Issue 2, pp.1775-1787
      !Critical mass for halo collapse with LW feedback and baryonic streaming
      real, intent(in) :: z ! redshift
      real J21, logM0, s

      J21 = 10.0**(2.0-z/5.0) !according to fit in Greif&Bromm 2006

      if (lw_mode .eq. 5) then!Schauer+21
        logM0 = 6.0174 * (1.0 + 0.166 * sqrt(J21)) !Eq.9
        get_Mcrit_Schauer = 10.0**(logM0 + 0.4159 * VBC)
      elseif (lw_mode .eq. 6) then!Schauer+21 (not tested)
        logM0 = 5.562 * (1.0 + 0.279 * sqrt(J21)) !Eq.11
        s = 0.614 * (1.0 - 0.560 * sqrt(J21))
        get_Mcrit_Schauer = 10.0**(logM0 + s * VBC)
      else
        write(0,*)"WARNING: lw_mode not compatible with Mcrit"
      endif
      return
  end function get_Mcrit_Schauer


  real function get_atomic_cooling_mass(z)
      !the atomic cooling mass is the virial mass of 10^4 K
      real, intent(in) :: z ! redshift
      get_atomic_cooling_mass = get_M_vir(z, 1.0e4) !in solar masses
      return
  end function get_atomic_cooling_mass

  real function get_M_vir(z, T)
      !function determines the halo mass corresponding to a virial temperature
      !we use the definition of  Hummel et al 2012
      real, intent(in) :: z ! redshift
      real, intent(in) :: T ! virial temperature
      get_M_vir = 1.e6*(T/1.e3)**(1.5) * (0.1*(z+1))**(-1.5)
      return
  end function get_M_vir

  real function get_T_vir(M,z) result(T)
      real, intent(in) :: M, z
      T  = 1000.0*(M*1.0e-6)**(2.0/3.0)*(1+z)*0.1
      return
  end function
end module crit_masses
