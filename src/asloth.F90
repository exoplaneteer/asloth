!code originally based on Parkinson et al. 2008
!Modified by Tilman Hartwig (2015) to determine the number of Pop III survivors
#include "asloth.h"

program ASLOTH

  use config
  use Defined_Types ! defined_types.F90
  use EPS_wrapper
  use Tree_Memory_Arrays_Passable ! memory_modules.F90 ! MergerTree in here
  use Numerical_Parameters 
  use resolution_parameters
  use to_file !to_file.F90
  use input_files 
  use Check_Flags
  use utility
  use volume_fractions
  use feedback_routines, only: node_feedback
  use bubble_tree, only: start_btree
  use tree_reader
  use tree_initialization
  use star_formation
  use read_yields
#ifdef METAL_YIELDS
  use metal_functions
#endif
  use populations
  use SF_routines
  use crit_masses, only: set_mcrit
  use cosmic_time, only: set_cosmic_time
  implicit none

  intrinsic GETARG
  character(len=200) cfg_name

  type (TreeNode), pointer :: This_Node, Dummy_Node
  integer :: i_omp, j, j_current !loop indices

  type(file_specs) :: output

  ! for parallel star formation & feedback loops
  integer, dimension(nlev_max) :: N_jcurrent ! Number of Nodes per timestep

  call GETARG(1,cfg_name)

  call set_default_config()
  call read_config(cfg_name)

  call Check_Compiler_Flags

  call init_outputs

  call set_cosmic_time
  call set_mcrit

#ifdef NBODY
  ! Reading the merger tree from the file
  call read_tree(number_of_nodes, tree_mass)
#else
  write(stdout,*)'Selected tree mass:', tree_mass
  write(stdout,*) 'mass resolution:',mres
  call make_EPS_tree()
  write(stdout,*) 'number of nodes:',number_of_nodes
#endif

  !Write main parameters to stdout to check if parameters are set as intended
  write(stdout,*) ' '
  write(stdout,*) 'Parameters used in this run:'
  write(stdout,*) 'Pop III IMF:    Mmin:',IMF_min,' Mmax:',IMF_max,' slope:',slope
  write(stdout,*) 'F_ESCII:',F_ESCII,' F_ESCIII:',F_ESCIII
  write(stdout,*) 'ETAII:',ETAII,' ETAIII:',ETAIII
  write(stdout,*) 'ALPHAOUTFLOW:',alpha_outflow,' MCHAOUT',m_cha_out
  write(stdout,*) ' '

  call init_stellar_populations()

  call assign_yields()

#ifdef DEBUG
  call output_pop(popII, trim(output_string)//'/PopII_val.dat')
  call output_pop(popIII, trim(output_string)//'/PopIII_val.dat')
#endif

  ! configuring output routine
  output%out_dir = output_string
  output%stuff(1, :) = zlev


  V_COM = tree_mass*MSUN_MPC3_cgs/RHO_m_cgs
  L_BOX = V_COM**(1.0/3.0)!effective volume side length, cMpc
#ifdef DEBUG
  write(stdout,*) 'Estimated comoving Volume: ', V_com, "MPC³"
  write(stdout,*) "Comoving linear size: ", L_BOX, "MPC"
#endif

#ifndef NO_XYZ
  call start_btree ! initializing tree search for feedback bubbles
#endif


  !! Initialize Tree !!
  call init_tree(number_of_nodes, N_jcurrent)
  j = 1
  !!!!! STAR FORMATION & FEEDBACK LOOP !!!!!
  write(stdout,*) "Starting main star formation and feedback loop"
  do j_current=nlev, 1, -1 ! loop over time-steps
    call print_bar(real(j)/real(number_of_nodes), &
        & " Current redshift: "//real_to_string3(zlev(j_current))//" PROGRESS: ")
#ifdef MEM_REPORT
    call mem_report 
#endif
#ifndef NO_XYZ
    call clean_tree(j_current)
#endif
    if (j_current .lt. nlev) then
      j = j + N_jcurrent(j_current+1)
      call get_fracs(j_current+1)
    endif 

    ! checking how each node in this step is affected by feedback
    !$OMP PARALLEL DO PRIVATE(i_omp, This_Node)     
    do i_omp=j, j+N_jcurrent(j_current)-1
      This_Node => MergerTree(number_of_nodes-i_omp+1)
      call node_feedback(This_Node) ! in feedback_routines.F90
      if (This_Node%i_mcrit) then
        This_Node%i_SF = .true. ! Should this halo call star formation?
        ! this needs to be known before the next loop starts to avoid double star formation
      endif
    end do
    !$OMP END PARALLEL DO

    ! star formation loop
    !$OMP PARALLEL DO PRIVATE(i_omp, This_Node)
    do i_omp=j, j+N_jcurrent(j_current)-1
      This_Node => MergerTree(number_of_nodes-i_omp+1)
      if (This_Node%i_SF .and. This_Node%jlevel .gt. 1) then
        call SF_step(This_Node)
      endif
    end do
    !$OMP END PARALLEL DO

    ! feedback-tracking loop
    do i_omp=j, j+N_jcurrent(j_current)-1
      This_Node => MergerTree(number_of_nodes-i_omp+1)
      if (This_Node%i_SF .and. This_Node%jlevel .gt. 1) then
        call add_parent_to_tree(This_Node)
      endif
      if (This_Node%jlevel .lt. nlev) call add_to_SFR(This_Node)
      call add_to_feedback_volumes(This_Node) 
      if (associated(This_Node%parent)) then ! inherit different quantities
        Dummy_Node => This_Node%parent
#ifdef OUTPUT_GAS_BRANCH
        if(Dummy_Node%id == NextOutputID .or. NextOutputID == -1)then
          ! -1: use the first star-forming node to start tracing
          if(associated(Dummy_Node%parent)) then !because SF happended in parent node
            NextOutputID = Dummy_Node%parent%id
          endif
        endif
#endif

#if (defined(MDF))
        if(associated(This_Node%This_Array))then
#ifdef MDF
          if(allocated(This_Node%This_Array%MDF_array)) then
            !Inherit MDF to the base node at the final redshift
            call add_to_Base_MDF(This_Node)
          endif
#endif
        endif
#endif
        call add_to_Base_MstarIISurv(This_Node)
#ifdef NO_XYZ
        if(This_Node%i_checked) Dummy_Node%i_checked = .true.
#endif
      end if !parent
    end do !feedback tracing loop
  end do ! loop over time-steps
  call print_bar(1.0, " Current redshift: "//real_to_string3(zlev(1))//" PROGRESS: ", & 
      & complete=.True.)


  call get_fracs(1)

  !!!only  outputs below this point !!!
  call write_files_reali()
#ifdef Satellites
  call outputs_satellites(number_of_nodes)
#endif
  call write_MW_properties()
  call write_files(output)
  call close_outputs

  write(stdout,*)"A-SLOTH finished successfully."
end program ASLOTH
