#include "../asloth.h"
module trace_MDF
  use defined_types
  implicit none

  real :: MDFZmin, MDFZmax, MDFdZ ! MDF range, bin width
  parameter (MDFZmin =  -9)
  parameter (MDFZmax =   1)
  parameter (MDFdZ   = 0.1)
  !the bin centres are in the middle of a bin
  !e.g. the first bin is at -8.95 and includes metallicities [-9,-8.9)
  integer :: MDFnpad, MDFnbin
  parameter (MDFnpad = 2) !add two bins to track outside min/max
  real, dimension(:), protected, allocatable :: MDF_Zarray

#ifdef MDF
  contains
    subroutine init_MDF()
      real :: Z_now

      MDFnbin = INT((MDFZmax-MDFZmin)/MDFdZ)
      allocate(MDF_Zarray(MDFnbin+MDFnpad))
      MDF_Zarray(1) = MDFZmin-0.5*MDFdZ
      MDF_Zarray(MDFnbin+MDFnpad) = MDFZmax+0.5*MDFdZ
      Z_now = MDFZmin+0.5*MDFdZ
      do while(Z_now < MDFZmax)
        MDF_Zarray(get_MDF_bin(Z_now)) = Z_now
        Z_now = Z_now + MDFdZ
      enddo
    end subroutine


    integer function get_MDF_bin(Zgas)![Fe/H]
      real :: Zgas
      integer :: binZ
      if (Zgas .lt. MDFZmin) then
        binZ = 1
      elseif (Zgas .ge. MDFZmax) then
        binZ = MDFnbin + MDFnpad
      else
        binZ = INT( (Zgas-MDFZmin)/(MDFZmax-MDFZmin)*float(MDFnbin) ) + MDFnpad
      endif
      get_MDF_bin = binZ
        
    end function get_MDF_bin


    subroutine add_to_Node_MDF(This_Node,MDF_Nstar,MDF_index,idx2)
      !add MDF_Nstar stars to the MDF of This_Node
      type(TreeNode), intent(inout) :: This_Node
      real, intent(in) :: MDF_Nstar
      integer, intent(in) :: MDF_index, idx2
      !idx2 to decide which special array it should be added to
      if(.not. associated(This_Node%This_Array)) then
        allocate(This_Node%This_Array)
      endif
      !This_Array can exist, but MDF_array not necessarily
      if(.not. allocated(This_Node%This_Array%MDF_array)) then
        allocate(This_Node%This_Array%MDF_array(MDFnbin+MDFnpad,1))
        This_Node%This_Array%MDF_array(:,:) = 0
      endif

      This_Node%This_Array%MDF_array(MDF_index,idx2) = &
      This_Node%This_Array%MDF_array(MDF_index,idx2) + MDF_Nstar
    end subroutine add_to_Node_MDF

    subroutine add_to_Base_MDF(This_Node)
      !Inherit MDF to the base node at the final redshift
      type(TreeNode), intent(inout) :: This_Node
      type (TreeNode), pointer :: Base_Node
      Base_Node => This_Node%base

      if(.not. associated(Base_Node%This_Array)) then
        allocate(Base_Node%This_Array)
      endif
      if(.not. allocated(Base_Node%This_Array%MDF_array)) then
          allocate(Base_Node%This_Array%MDF_array(MDFnbin+MDFnpad,1))
          Base_Node%This_Array%MDF_array(:,:) = 0
      endif
      Base_Node%This_Array%MDF_array = &
      Base_Node%This_Array%MDF_array + This_Node%This_Array%MDF_array

      if(This_Node%jlevel>1)then
        deallocate(This_Node%This_Array%MDF_array)
      endif
    end subroutine add_to_Base_MDF


#endif
end module
