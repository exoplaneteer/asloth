#include "../asloth.h"
module read_yields
  ! module for reading yields from the appropirate files
  use populations
  use config, only: config_file
  use metals
  implicit none
contains
#ifdef METAL_YIELDS
  subroutine assign_yields()
#if defined(METAL_YIELDS)
    write(stdout,*)'Assigning yields'
    ! We first get which elements we want.
    call ReadElementID()
    ! We assign the PopIII and PopII yields to an array.
    call assign_yields_general(popiii_yields_file, popIII)
    call assign_yields_general(popii_yields_file, popII)
#else
    N_ELEMENTS = 1 !needs to be defined even without METAL_YIELDS
    call popII%set_yields()
    call popIII%set_yields()
#endif
  end subroutine assign_yields


subroutine ReadElementID()
    character(LEN=5), dimension(:), allocatable :: ElementName
    integer                        :: i
    integer                        :: iunit

    type(element) :: this_element
    namelist /n_ele/ N_ELEMENTS
    namelist /config_metals/ ElementName


    open(newunit=iunit, FILE=config_file, STATUS="OLD")
    read(iunit,nml=n_ele)
    if (.not. allocated(ElementName)) allocate( ElementName(N_ELEMENTS-1) ) 
    read(iunit,nml=config_metals)
    close(unit=iunit)

#ifdef DEBUG
    write(stdout, *) "Number of elements wanted: ", N_ELEMENTS-1
#endif

    ! read in which elements we want
    do i=1 ,N_ELEMENTS-1
      this_element = element(ElementName(i))
      call tracked_elements%append(this_element)
    enddo
    index_carbon = tracked_elements%get_element_index("C")
    index_iron   = tracked_elements%get_element_index("Fe")
end subroutine ReadElementID

subroutine assign_yields_general(fileYields, pop)

    character,  intent(in)         :: fileYields*1024
    type(population), intent(in) :: pop !stellar population to which the yields will be assigned


    integer                        :: i, j, k
    integer                        :: index_close
    integer                        :: iunit, fileend, reason
    real                           :: dmass,dm

    character(LEN=8) :: M ! parameter name
    integer :: mass_num ! number of masses in the table  
    integer :: num_max_ele ! maximum number of elements
    ! temporary arrays for for reading and converting yields
    real, dimension(:,:), allocatable :: Metal_Yields_Table
    real, dimension(:,:), allocatable :: Metal_Yields, Metal_Yields_tmp ! metal yields in the entire table
    character(LEN=3), dimension(:), allocatable :: Element_Names
    integer, dimension(:), allocatable :: mass_number
    real, dimension(:), allocatable :: Marr ! arrays

    type(element) :: this_element
    
    call get_elem_num(fileYields, num_max_ele, mass_num)

    allocate(Element_Names(num_max_ele))
    allocate(Mass_Number(num_max_ele))
    allocate(Marr(mass_num))
    allocate(Metal_Yields_Table(num_max_ele,mass_num))
    allocate(Metal_Yields_tmp(N_ELEMENTS, mass_num))
    allocate(Metal_Yields(pop%N_bins, N_ELEMENTS) )

    Metal_Yields_tmp(:,:) = 1.0e-20
    metal_yields = 1e-20

    open (newunit=iunit, file=fileYields, status='old')
    read (iunit,*,iostat=reason) M, Marr
    read (iunit,*,iostat=reason) ! skipping remnant masses

    do i=1, num_max_ele
      read (iunit,*,iostat=fileend) Element_Names(i), mass_number(i), Metal_Yields_Table(i,:)
    enddo
    close(iunit)
    Metal_Yields_Table(:, :) = ABS( Metal_Yields_Table(:, :) ) ! there are negative values in some of the tables

    do i=1, num_max_ele ! element
      do j=1, mass_num ! mass
        if (Metal_Yields_Table(i,j) .lt. 1e-20) then
          Metal_Yields_Table(i,j) = 1e-20
        endif
        if (mass_number(i) .ge. 6) then
          Metal_Yields_tmp(N_ELEMENTS,j) = Metal_Yields_tmp(N_ELEMENTS,j) + Metal_Yields_Table(i, j)
        endif
        do k=1, N_ELEMENTS-1
          this_element = tracked_elements%list(k)
          if (This_element%Element_Name .eq. Element_Names(i)) then
              Metal_Yields_tmp(k,j) = Metal_Yields_tmp(k,j) + Metal_Yields_Table(i, j)
            endif
          end do
        enddo
      enddo 
      do j=1, mass_num
        if (Marr(j) .lt. 1e-20) then
          Marr(j) = 1e-20
        endif
      enddo


      do i=1, pop%N_bins !go through IMF bins
        if(pop%mass(i) < M_CCSN_MIN .or.&
            (pop%mass(i) > M_CCSN_MAX .and. pop%mass(i) < M_PISN_MIN) .or.&
            pop%mass(i) > M_PISN_MAX) then
          ! mass outside SN ranges: no metals
          Metal_Yields(i,:) = 1.0e-20
        else ! find closest yield model
          ! To start with
          dmass = 10000 !Maximum range
          index_close = -1 !to raise error when not found
          !find closest
          do j=1,mass_num ! go through available x-data
            dm = abs(pop%mass(i)-Marr(j)) ! distance to nearest grid point
            if (dm < dmass) then
              if (SUM(Metal_Yields_Table(:,j)) > 1e-10) then ! make sure that we only use tabulated masses that provide metal yields
                dmass = dm
                index_close = j
              endif
            endif
          enddo
          if(index_close < 0) then
            write(0,*)"ERROR: closest index not found!", pop%mass(i)
          endif

          !Fix cases in which the interpolarion tries to get a model outside of the SN ranges
          if(pop%mass(i) > M_CCSN_MIN .and. Marr(index_close) < M_CCSN_MIN) index_close=index_close+1
          if(pop%mass(i) < M_CCSN_MAX .and. Marr(index_close) > M_CCSN_MAX) index_close=index_close-1
          if(pop%mass(i) > M_PISN_MIN .and. Marr(index_close) < M_PISN_MIN) index_close=index_close+1
          if(pop%mass(i) < M_PISN_MAX .and. Marr(index_close) > M_PISN_MAX) index_close=index_close-1

          Metal_Yields(i, :) = Metal_Yields_tmp(:,index_close) 
        endif ! interpolation
      enddo ! end through mass bin

      call pop%set_yields(Metal_Yields)

      deallocate(Element_Names)
      deallocate(mass_number)
      deallocate(Marr)
      deallocate(Metal_Yields_Table)
      deallocate(Metal_Yields_tmp)
      deallocate(Metal_Yields)

  end subroutine assign_yields_general
#endif

  subroutine get_elem_num(f, num_ele, mass_num)
      ! defines the number of elements and the number of different stellar masses in the yields file
      ! hard-coded file names. If you want to use a different table, add it here.
      character(len=1024),  intent(in) :: f !file name
      integer, intent(out) :: mass_num ! number of masses in the table  
      integer, intent(out) :: num_ele ! number of elements in the file
      if (f == "Data/nomoto_Z1e-3.txt") then
        num_ele = 83
        mass_num = 27
      endif
      if (f == "Data/kobayashi2006_Z1e-3.txt") then
        num_ele = 83
        mass_num = 7
      endif
      if (f == "Data/yields_isotopes_sum_TR.dat") then
        num_ele = 32
        mass_num = 39
      endif
  end subroutine

end module read_yields
