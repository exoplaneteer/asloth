#include "../asloth.h"
module bubble_tree
  !This module containes routines for adding feedback of haloes to the tracing system and for testing whether a halo or a specific point in space is affected by the feedback
  use numerical_parameters
  use defined_types
  use config, only: cubic_box
  use cosmic_time, only: alev, a3_inv
  use crit_masses, only: Atomic_Cooling_Mass
#ifdef METAL_YIELDS
  use metal_functions
#endif
  use bubble_tree_types
  implicit none
  
  ! pointer type for nodes of the bubble tree. Needed to make pointer arrays

  !maximum depth of tree
  integer, parameter :: max_level=20

  ! box size in com. Mpc.
  ! chose something sufficiently large if you don't know
  real :: L_box_max
  ! structure for pointer for top level nodes
  type(n_pointer), dimension(nlev_max) :: Head_nodes, Head_nodes_ion

  real :: Mass_diff

!bubbles require spatial information

contains

  subroutine start_btree()
      ! initializes tree and sets up the head nodes
      integer :: i
      type(bubble_node), pointer :: tmp_Node
      if (cubic_box) then
        L_box_max = 1.1*L_Box/HUBBLE
      else
        l_box_max = 1000.0
      endif

      do i= 1,nlev
        allocate(Head_Nodes(i)%p)
        tmp_Node => Head_Nodes(i)%p
        tmp_Node%level = 1  ! top level box
        tmp_Node%x_node(:) = 0.0 ! set node corner at (0,0,0)
        tmp_Node%L_box = L_Box_max*alev(i) ! convert box size to physical
        allocate(Head_Nodes_ion(i)%p)
        tmp_Node => Head_Nodes_ion(i)%p
        tmp_Node%level = 1  ! top level box
        tmp_Node%x_node(:) = 0.0 ! set node corner at (0,0,0)
        tmp_Node%L_box = L_Box_max*alev(i) ! convert box size to physical
      end do
  end subroutine start_btree


#ifndef NO_XYZ
  subroutine add_bubble_to_tree(NodeAdd, Mmetal_add)
      ! Adds ionized an enriched bubble of a node to the tree
      type(TreeNode), intent(inout) :: NodeAdd
      real, dimension(N_ELEMENTS), intent(in) :: Mmetal_add !metal mass in this SN shell
      type(bubble_node), pointer :: BNode ! Node of the bubble, node where it previously was
      real :: R ! effective radius according to which the Node is selected.
      R = max(NodeAdd%r_en, NodeAdd%R_ion)
      if (R .eq. 0.0) return  ! don't add bubbles with R=0. Those won't do anything

      call find_bnode(NodeAdd%jlevel, NodeAdd%x, BNode, R) ! Find correct node of tree
      call increase_size(BNode)
      call copy_props_to_bubble(BNode, BNode%N_bub, NodeAdd, Mmetal_add)
      
      call find_bnode(NodeAdd%jlevel, NodeAdd%x, BNode, NodeAdd%R_ion, ion_tree=.True.) ! Find correct node of tree
      call increase_size(BNode)
      call copy_props_to_bubble(BNode, BNode%N_bub, NodeAdd, Mmetal_add)
  end subroutine add_bubble_to_tree


  ! This subroutine looks for the smallest node that fully encompasses the Bubble
  subroutine find_bnode(jlevel, x, BNode, R, ion_tree)
      integer, intent(in) :: jlevel
      real, intent(in) :: x(3)
      logical, optional, intent(in) :: ion_tree
      logical :: use_ion_tree
      real, intent(in) :: R
      type(bubble_node), pointer, intent(out) :: BNode
      type(bubble_node), pointer :: sub_node
      integer, dimension(3) :: ijk1, ijk2
      integer :: i, j

      if (present(ion_tree)) then
        use_ion_tree = ion_tree
      else 
        use_ion_tree = .False.
      endif
      if (.not. use_ion_tree) then
        BNode => Head_Nodes(jlevel)%p ! starting at top-level node
      else
        BNode => Head_Nodes_ion(jlevel)%p ! starting at top-level node
      endif

      do
        ! compute which sub-nodes the bubble overlaps with
        do i=1,3
          ijk1(i) = int(2*(x(i) - R - BNode%x_node(i))/BNode%L_box + 1.0) - 1
          ijk2(i) = int(2*(x(i) + R - BNode%x_node(i))/BNode%L_box + 1.0) - 1
        end do
        ! turn three indices (0 or 1) into one unique index from 1-8 to access the correct subnode
        i = 1 + ijk1(1) + 2*ijk1(2) + 4*ijk1(3)
        j = 1 + ijk2(1) + 2*ijk2(2) + 4*ijk2(3)
        if (i .eq. j) then ! if both limits want to access the same sub-node, go there
          ! make sure nothing went wrong, in particular, no index should be .gt. 1
          call grid_check(ijk1, BNode, x, "find_bnode")
          if (.not. associated(BNode%sub_nodes(i)%p)) then ! if sub-node doesn't exist, make it
            allocate(BNode%sub_nodes(i)%p)
            sub_node => BNode%sub_nodes(i)%p
            sub_node%x_node(:) = BNode%x_node(:) + 0.5*ijk1(:)*BNode%L_box !position of the corner of the node
            sub_node%L_box = BNode%L_box*0.5 ! edge length of the node
            sub_node%level = BNode%level + 1
            sub_node%parent_node => BNode ! associate parent node
          endif
          BNode => BNode%sub_nodes(i)%p ! repeat with sub node
        else ! if the bubble is in several sub-nodes, we're done
          exit
        endif
        if (BNode%level .gt. max_level) then ! limit the depth of the tree
          exit
        endif
      end do
  end subroutine find_bnode 

  ! Subroutine to remove old bubble and fix the pointers
  subroutine rm_bubble(node, to_remove)
      integer :: to_remove
      type(bubble_node), pointer :: node

      call mv_bubble(node, to_remove, node, node%N_bub)
      node%pop(node%N_bub) = 0
      node%r_en(node%N_bub) = 0
      node%R_ion(node%N_bub) = 0
      node%L_ion(node%N_bub) = 0
      node%Mmetals_bub(node%N_bub) = 0
      node%x(:, node%N_bub) = 0
#ifdef METAL_YIELDS
      node%yields_bub(:, node%N_bub) = 0
#endif
      node%N_bub  = node%N_bub - 1

  end subroutine rm_bubble


  subroutine mv_bubble(n_from, i_from, n_to, i_to)
      ! moves the bubble to_move to the node move_to
      ! and fixes all the pointers afterwards
      integer, intent(in) :: i_from, i_to
      type(bubble_node), pointer, intent(inout) :: n_from, n_to
      integer :: i_tmp
      real :: r_tmp
      real, dimension(3) :: x_tmp
      real, dimension(:), allocatable :: met_tmp

      i_tmp = n_from%pop(i_from)
      n_from%pop(i_from) = n_to%pop(i_to)
      n_to%pop(i_to) = i_tmp

      r_tmp = n_from%r_en(i_from)
      n_from%r_en(i_from) = n_to%r_en(i_to)
      n_to%r_en(i_to) = r_tmp

      r_tmp = n_from%R_ion(i_from)
      n_from%R_ion(i_from) = n_to%R_ion(i_to)
      n_to%R_ion(i_to) = r_tmp
      
      r_tmp = n_from%L_ion(i_from)
      n_from%L_ion(i_from) = n_to%L_ion(i_to)
      n_to%L_ion(i_to) = r_tmp
      
      r_tmp = n_from%Mmetals_bub(i_from)
      n_from%Mmetals_bub(i_from) = n_to%Mmetals_bub(i_to)
      n_to%Mmetals_bub(i_to) = r_tmp
      
      x_tmp = n_from%x(:,i_from)
      n_from%x(:, i_from) = n_to%x(:,i_to)
      n_to%x(:, i_to) = x_tmp
#ifdef METAL_YIELDS
      if (.not. allocated(met_tmp)) allocate(met_tmp(N_ELEMENTS))
      met_tmp = n_from%yields_bub(:,i_from)
      n_from%yields_bub(:, i_from) = n_to%yields_bub(:,i_to)
      n_to%yields_bub(:, i_to) = met_tmp
#endif
  end subroutine mv_bubble

  subroutine copy_props_to_bubble(BNode, Bubble, NodeAdd, Mmetal_add)
      type(TreeNode), intent(inout) :: NodeAdd
      real, dimension(:), intent(in) :: Mmetal_add !metal mass in this SN shell
      integer, intent(in) :: Bubble ! Index of Bubble
      type(bubble_node), pointer :: BNode

      BNode%r_en(Bubble) = NodeAdd%r_en 
      BNode%x(:, Bubble) = NodeAdd%x(:)
      BNode%R_ion(Bubble) = NodeAdd%R_ion
      BNode%L_ion(Bubble) = NodeAdd%L_ion
      BNode%M_out(Bubble) = NodeAdd%M_out
#ifdef METAL_YIELDS
      BNode%Mmetals_bub(Bubble) = Mmetal_add(N_ELEMENTS)
      BNode%yields_bub(:, Bubble) = Mmetal_add(:)
#endif
      BNode%pop(Bubble) = NodeAdd%pop

      return
  end subroutine copy_props_to_bubble

  subroutine bubble_check(This_Node)
      ! routine to check whether This_Node is externally enricher and/or ionized
      type(TreeNode), target, intent(inout) :: This_Node
      real :: R_dist
      real, dimension(3) :: x_dist ! halo distance vector
      type(bubble_node), pointer :: BNode
      integer :: Bubble
      integer, dimension(3) :: ijk
      integer :: k
      logical, allocatable :: do_this(:) 
      integer :: n_max
#ifdef METAL_YIELDS
      real, allocatable :: Z_dens(:)
      real :: M_dens_av, M_dens_add, V_inv
      integer :: N_av
      M_dens_add = 0.0
      M_dens_av = 0.0
      N_av = 0
      allocate(Z_dens(N_ELEMENTS))
      Z_dens(:) = 0
      if (allocated(This_Node%IGM_Z)) then
        This_Node%IGM_Z(:) = 0
      endif
#endif

      BNode => Head_Nodes(This_Node%jlevel)%p ! start with head node

      n_max = 1000
      allocate(do_this(1000))

      do ! continue this until we ran out of bubbles and nodes
        if (BNode%N_bub .gt. n_max) then
          deallocate(do_this)
          n_max = BNode%N_bub
          allocate(do_this(n_max))
        end if
        do concurrent (Bubble=1:BNode%N_bub)
          do_this(Bubble) = dist_check_allow_same(This_Node%x, BNode%x(:, Bubble), BNode%R_en(Bubble))
        end do
        do Bubble=1, BNode%N_bub
          if (.not. do_this(Bubble)) cycle
          x_dist = BNode%x(:, Bubble) - This_Node%x(:)
          R_dist = x_dist(1)*x_dist(1)+x_dist(2)*x_dist(2)+x_dist(3)*x_dist(3)
          if (R_dist .lt. BNode%r_en(Bubble)*BNode%r_en(Bubble)) then !overlap
#ifndef METAL_YIELDS
!prev: ambient metallicity
            if (R_dist .eq. 0) cycle
#endif
            This_Node%i_enr = .True.

#ifdef METAL_YIELDS
            ! set metallicity but don't add metals
            V_inv = 3.0/(4*PI*BNode%r_en(Bubble)*BNode%r_en(Bubble)*BNode%r_en(Bubble))
            Z_dens(:) = Z_dens(:) + BNode%yields_bub(:, Bubble)*V_inv
            M_dens_add = M_dens_add + BNode%M_out(bubble)*V_inv
            M_dens_av = M_dens_av + RHO_b_AST*a3_inv(This_node%jlevel) + Omega_b/Omega_m*This_Node%Mhalo*V_inv
            N_av = N_av + 1
            if (.not. allocated(This_Node%IGM_Z)) then
              allocate(This_Node%IGM_Z(N_ELEMENTS))
              This_Node%IGM_Z(:) = 0
            endif
            This_Node%IGM_Z(:) = Z_dens(:)/(M_dens_add+M_dens_av/N_av)
#endif
          endif !overlap
        end do
        do k=1,3 ! find next sub-node to check
          ijk(k) = int(2.0*This_Node%x(k)/BNode%L_box + 1.0) - int(2.0*BNode%x_node(k)/BNode%L_box+0.5+1.0)
        end do
        k = ijk(1) + 2*ijk(2) + 4*ijk(3) + 1
        ! safety check
        call grid_check(ijk, BNode, this_node%x, "bubble_check")

        if (associated(BNode%sub_nodes(k)%p)) then ! if sub-node exists: continue checking there
          BNode => BNode%sub_nodes(k)%p
        else ! otherwise: done
          exit
        endif !sub-node exists
      enddo

#ifdef METAL_YIELDS
      deallocate(Z_dens)
#endif
  end subroutine bubble_check


  subroutine ion_check(This_Node)
      ! routine to check whether This_Node is ionized
      ! the treatment of haloes above the atomic cooling limit is based on Visbal et al. 2016 (MNRAS 460, L59-L63)
      type(TreeNode), target, intent(inout) :: This_Node
      real :: R_dist, F_ion 
      real, dimension(3) :: x_dist ! halo distance vector
      type(bubble_node), pointer :: BNode
      integer :: Bubble
      logical, allocatable :: do_this(:) 
      integer :: n_max
      integer, dimension(3) :: ijk
      integer :: k

       if (This_Node%mhalo .ge. 10*Atomic_Cooling_Mass(This_Node%jlevel)) then
         return ! no need to check this, halo is too massive to be affected with ionization
       endif

      BNode => Head_Nodes_ion(This_Node%jlevel)%p ! start with head node
      Bubble = 0
      n_max = 1000
      F_ion = 0
      allocate(do_this(1000))

      do ! continue this until we ran out of bubbles and nodes
        if (BNode%N_bub .gt. n_max) then
          deallocate(do_this)
          n_max = BNode%N_bub
          allocate(do_this(n_max))
        end if
        do concurrent (Bubble=1:BNode%N_bub)
          do_this(Bubble) = dist_check(This_Node%x, BNode%x(:, Bubble), BNode%R_ion(Bubble))
        end do
        do Bubble=1, BNode%N_bub
          if (.not. do_this(Bubble)) cycle
          x_dist = BNode%x(:, Bubble) - This_Node%x(:)
          R_dist = x_dist(1)*x_dist(1)+x_dist(2)*x_dist(2)+x_dist(3)*x_dist(3)
          if ((R_dist .lt. BNode%R_ion(Bubble)*BNode%R_ion(Bubble) .and. R_dist .gt. 0)) then
            This_Node%i_ion = .true.
            if (This_Node%mhalo .gt. Atomic_Cooling_Mass(This_Node%jlevel)) then
              This_Node%i_ion = .false.
              ! Large ionising flux suppressses Star formation Visbal et al. 2017
              F_ion = F_ion + BNode%L_ion(Bubble)/R_dist/MPC2CM/MPC2CM
              if (F_ion .gt. 6.7e6) then 
                This_Node%i_ion = .true.
              endif
            endif
          endif
        end do
        do k=1,3 ! find next sub-node to check
          ijk(k) = int(2.0*This_Node%x(k)/BNode%L_box + 1.0) - int(2.0*BNode%x_node(k)/BNode%L_box+0.5+1.0)
        end do
        k = ijk(1) + 2*ijk(2) + 4*ijk(3) + 1
        ! safety check
        call grid_check(ijk, BNode, this_node%x, "ion_check")

        if (associated(BNode%sub_nodes(k)%p)) then ! if sub-node exists: continue checking there
          BNode => BNode%sub_nodes(k)%p
        else ! otherwise: done
          exit
        endif !sub-node exists
      enddo
      deallocate(do_this)
  end subroutine ion_check

  subroutine clean_tree(jlevel)
      integer, intent(in) :: jlevel
      type(bubble_node), pointer :: BNode
      integer :: n, n_merge
      n = 1
      n_merge = 1
      ! repeat cleaning run until there is nothing left to be cleaned, i.e.,
      ! n_merge=0
      do while(n_merge .gt. 0)
        n_merge = 0
        BNode => Head_Nodes_ion(jlevel)%p
        call clean_BNode(jlevel, BNode, n_merge)
        n = n+1
      end do
  end subroutine

  recursive subroutine clean_BNode(jlevel, BNode, n_merge)
    integer, intent(in) :: jlevel
    integer :: Bubble, k
    type(bubble_node), pointer, intent(inout) :: BNode
    type(bubble_node), pointer :: BNode_new
    real :: x_tmp(3), V_tmp, L_tmp, V_in, x_in(3)
    integer, intent(inout) :: n_merge
    do k=1, 8
        if (associated(BNode%sub_nodes(k)%p)) then ! if sub-node exists: continue checking there
          call clean_Bnode(jlevel, BNode%sub_nodes(k)%p, n_merge)
        endif
    end do
    k = 1
    do while(k .le. BNode%N_bub)
      V_in = BNode%R_ion(k)*BNode%R_ion(k)*BNode%R_ion(k)
      x_in = BNode%x(:,k)
      call Merge_into_Bubble(jlevel, x_in, V_in, BNode%L_ion(k), x_tmp, V_tmp, L_tmp, &
          & BNode, k)
      if (V_in .ne. V_tmp) then
        n_merge = n_merge + 1
        BNode%x(:,k) = x_tmp 
        BNode%R_ion(k) = V_tmp ** (1.0/3.0)
        BNode%L_ion(k) = L_tmp
        call find_bnode(jlevel, x_tmp, BNode_new, BNode%R_ion(k), ion_tree=.True.) ! Find correct node of tree
        if (.not. associated(BNode, BNode_new)) then
            BNode_new%N_bub = BNode_new%N_bub + 1 ! after moving, the bubble is at the last position of BNode
            Bubble = BNode_new%N_bub ! after moving, the bubble is at the last position of BNode
            if (BNode_new%N_bub .gt. BNode_new%N_max) call BNode_new%resize() ! resizing if needed
            call mv_bubble(BNode, k, BNode_new, Bubble) ! move the bubble
            call rm_bubble(BNode, k) ! remove old bubble
          endif
      endif
      k = k + 1
    end do
  end subroutine clean_BNode

  recursive subroutine Merge_into_Bubble(jlevel, x_in, V_in, L_in, x_out, V_ion_current, L_out, BNode, Bubble_in)
      ! routine to check whether This_Node is externally enricher and/or ionized
      integer, intent(in) :: jlevel
      real, intent(in) :: x_in(3), V_in, L_in
      real, intent(out) :: x_out(3), V_ion_current, L_out
      type(bubble_node), pointer :: BNode
      integer :: Bubble
      integer, intent(in) :: Bubble_in
      integer :: n_max
      integer, dimension(3) :: ijk
      logical, allocatable :: do_this(:) 
      integer :: k
      real :: R_in
      real :: V_current

      n_max = 1000
      allocate(do_this(1000))
      x_out(:) = x_in(:)
      V_ion_current = V_in
      L_out = L_in
      R_in = V_in**(1.0/3.0)

      do k=1,3 ! find next sub-node to check
        ijk(k) = int(2.0*x_in(k)/BNode%L_box + 1.0) - int(2.0*BNode%x_node(k)/BNode%L_box+0.5+1.0)
      end do
      k = ijk(1) + 2*ijk(2) + 4*ijk(3) + 1
      ! safety check
      call grid_check(ijk, BNode, x_in, "Merge_into_bubble")

      if (associated(BNode%sub_nodes(k)%p)) then ! if sub-node exists: continue checking there
        call Merge_into_Bubble(jlevel, x_in, V_in, L_in, x_out, V_ion_current, L_out, BNode%sub_nodes(k)%p, 1)
      endif

      if (BNode%N_bub .gt. n_max) then
        deallocate(do_this)
        n_max = BNode%N_bub
        allocate(do_this(n_max))
      end if
      do concurrent (Bubble=1:BNode%N_bub) ! checking for overlap
        do_this(Bubble) = dist_check(x_in, BNode%x(:, Bubble), BNode%R_ion(Bubble))
        do_this(Bubble) = (do_this(Bubble) .or. dist_check(x_in, BNode%x(:, Bubble), R_in))
      end do
      ! Merging bubbles
      do Bubble=Bubble_in, BNode%N_bub
        if (.not. do_this(Bubble)) cycle
        L_out = L_out + BNode%L_ion(Bubble)
        V_current = BNode%R_ion(Bubble)*BNode%R_ion(Bubble)*BNode%R_ion(Bubble)
        x_out(:) = (x_out(:)*V_ion_current + BNode%x(:, Bubble)*V_current)/(V_ion_current + V_current)
        V_ion_current = V_ion_current + V_current
      end do
      ! removing all merged bubbles (back to front)
      do Bubble=BNode%N_bub, Bubble_in, -1
        if (.not. do_this(Bubble)) cycle
        call rm_bubble(BNode, Bubble)  
      end do
      deallocate(do_this)
  end subroutine merge_into_bubble

  subroutine position_check(x, j_now, is_ion, is_enr)
      ! routine to check whether This_Node is externally enricher and/or ionized
      real, intent(in) :: x(3)
      integer, intent(in) :: j_now
      logical, intent(out) :: is_ion, is_enr

      real, dimension(3) :: x_dist ! halo distance vector
      real :: R_dist
      type(bubble_node), pointer :: BNode
      integer :: Bubble
      integer, dimension(3) :: ijk
      integer :: k

      is_ion = .False.
      is_enr = .False.

      BNode => Head_Nodes(j_now)%p ! start with head node
      do ! continue this until we ran out of bubbles and nodes
        do Bubble=1,BNode%N_bub
          x_dist = BNode%x(:, Bubble) - x(:)
          R_dist = x_dist(1)*x_dist(1)+x_dist(2)*x_dist(2)+x_dist(3)*x_dist(3)
          if ((R_dist .lt. BNode%R_ion(Bubble)*BNode%R_ion(Bubble) .and. R_dist .gt. 0)) then
            is_ion = .true.
          endif
          if ((R_dist .lt. BNode%R_en(Bubble)*BNode%R_en(Bubble))&
              & .and. (R_dist .gt. 0)) then !overlap
            is_enr = .True.
          endif!overlap
        end do
        if (is_ion .and. is_enr) exit ! no need to check further
        do k=1,3 ! find next sub-node to check
          ijk(k) = int(2.0*x(k)/BNode%L_box + 1.0) - int(2.0*BNode%x_node(k)/BNode%L_box+0.5+1.0)
        end do
        k = ijk(1) + 2*ijk(2) + 4*ijk(3) + 1
        ! safety check
        call grid_check(ijk, BNode, x, "position_check")

        if (associated(BNode%sub_nodes(k)%p)) then ! if sub-node exists: continue checking there
          BNode => BNode%sub_nodes(k)%p
        else ! otherwise: done
          exit
        endif !sub-node exists
      enddo
  end subroutine position_check

  function count_ion(x, j_now, ion_tree) result(n_ion)
      ! routine to check whether This_Node is externally enricher and/or ionized
      real, intent(in) :: x(3)
      integer, intent(in) :: j_now
      logical, intent(in) :: ion_tree
      integer :: n_ion

      real, dimension(3) :: x_dist ! halo distance vector
      real :: R_dist
      type(bubble_node), pointer :: BNode
      integer :: Bubble
      integer, dimension(3) :: ijk
      integer :: k


      n_ion = 0
      
      if (.not. ion_tree) then
        BNode => Head_Nodes(j_now)%p ! starting at top-level node
      else
        BNode => Head_Nodes_ion(j_now)%p ! starting at top-level node
      endif
      
      do ! continue this until we ran out of bubbles and nodes
        do Bubble=1,BNode%N_bub
          x_dist = BNode%x(:, Bubble) - x(:)
          R_dist = x_dist(1)*x_dist(1)+x_dist(2)*x_dist(2)+x_dist(3)*x_dist(3)
          if ((R_dist .lt. BNode%R_ion(Bubble)*BNode%R_ion(Bubble) .and. R_dist .gt. 0)) then
            n_ion = n_ion + 1
          endif
        end do
        do k=1,3 ! find next sub-node to check
          ijk(k) = int(2.0*x(k)/BNode%L_box + 1.0) - int(2.0*BNode%x_node(k)/BNode%L_box+0.5+1.0)
        end do
        k = ijk(1) + 2*ijk(2) + 4*ijk(3) + 1
        ! safety check
        call grid_check(ijk, BNode, x, "count_ion")

        if (associated(BNode%sub_nodes(k)%p)) then ! if sub-node exists: continue checking there
          BNode => BNode%sub_nodes(k)%p
        else ! otherwise: done
          exit
        endif !sub-node exists
      enddo
  end function count_ion

  ! checking whether ijk points to a valid sub-node
  subroutine grid_check(ijk, BNode, x_in, caller)
      integer, intent(in) :: ijk(3)
      type(bubble_node), pointer :: BNode
      real, intent(in) :: x_in(3)
      character(len=*), intent(in) :: caller
      if (maxval(ijk) .gt. 1 .or. minval(ijk) .lt. 0) then
        write(*,*) "ERROR: exceeded size of bubble-tree grid"
        write(*,*) "Caller:", caller
        write(*,*) "ijk = ", ijk
        write(*,*) "grid from ", BNode%x_node, " with width ", BNode%L_box
        write(*,*) "at level ", BNode%level
        write(*,*) "search positon: ", x_in
        BNode => BNode%parent_node
        write(*,*) "head node x", BNode%x_node, " with width ", BNode%L_box
        stop
      endif
  end subroutine

  !checking whether x and x_ref are closer than R
  logical pure function dist_check_allow_same(x_ref, x, R)
    real, dimension(3), intent(in) :: x_ref, x
    real, intent(in) :: R
    real, dimension(3) :: X_dist
    real :: R_dist
    dist_check_allow_same = .False.
    x_dist = x - x_ref
    R_dist = x_dist(1)*x_dist(1)+x_dist(2)*x_dist(2)+x_dist(3)*x_dist(3)
    if (R_dist .lt. R*R) dist_check_allow_same = .True.
  end function

  logical pure function dist_check(x_ref, x, R)
    real, dimension(3), intent(in) :: x_ref, x
    real, intent(in) :: R

    real, dimension(3) :: X_dist
    real :: R_dist
    dist_check = .False.
    x_dist = x - x_ref
    R_dist = x_dist(1)*x_dist(1)+x_dist(2)*x_dist(2)+x_dist(3)*x_dist(3)
    if (R_dist .lt. R*R .and. R_dist .gt. 0) dist_check = .True.
  end function
  

#endif
end module bubble_tree
